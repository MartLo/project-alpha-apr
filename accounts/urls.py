from django.urls import path
from accounts.views import account_view, user_logout, signup


urlpatterns = [
    path("login/", account_view, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup"),
]
