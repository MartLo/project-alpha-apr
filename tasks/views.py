from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task


# Create your views here.
@login_required
def task_view(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.is_completed = False
            task.save()
            return redirect("/projects/")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }

    return render(request, "tasks/create_task.html", context)


@login_required
def list_task(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/list_task.html", context)
