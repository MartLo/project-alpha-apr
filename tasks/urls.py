from tasks.views import task_view, list_task
from django.urls import path


urlpatterns = [
    path("create/", task_view, name="create_task"),
    path("mine/", list_task, name="show_my_tasks"),
]
